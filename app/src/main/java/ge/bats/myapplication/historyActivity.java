package ge.bats.myapplication;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class historyActivity extends AppCompatActivity {
    static Context context;
    private static final int DATE_DIALOG_ID = 1;
    private int year;
    private int month;
    private int day;
    EditText editTextDate;
    private String currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        context = this;
        setTitle("ჩემი ისტორია");




        TabLayout tabs = (TabLayout) findViewById(R.id.tabed);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here

                int position = tab.getPosition();
                switch (position) {
                    case 0:
                        startDraw(2);
                        break;
                    case 1:
                        startDraw(3);
                        break;
                    case 2:
                        startDraw(1);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        startDraw(2);
    }




    public void startDraw(Integer value) {
        String query = " WHERE `own`='"+ String.valueOf(value) +"'";

        ListView reading_list = (ListView) findViewById(R.id.history_list);
        List show = MainActivity.getResults(query);


        MyAdapter adapter = new MyAdapter(getBaseContext(),R.layout.list,(ArrayList) show);

        reading_list.setAdapter(adapter);
        reading_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent start = new Intent(historyActivity.this, userActivity.class);
                start.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                TextView list_code = (TextView) view.findViewById(R.id.list_code);
                TextView list_name = (TextView) view.findViewById(R.id.list_name);
                String code = list_code.getText().toString();
                String name = list_name.getText().toString();

                start.putExtra("name", name);
                start.putExtra("code", code);

                startActivity(start);
            };
         });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Context context = getApplicationContext();
        getMenuInflater().inflate(R.menu.history_menu, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if ( id == R.id.filter ) {

            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
            showDialog(DATE_DIALOG_ID);
        }
        return super.onOptionsItemSelected(item);
    }
    private void updateDisplay() {
        currentDate = new StringBuilder().append(day).append(".")
                .append(month + 1).append(".").append(year).toString();

        Log.i("DATE", currentDate);
    }
    DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker datePicker, int i, int j, int k) {

            year = i;
            month = j;
            day = k;
            updateDisplay();
            editTextDate.setText(currentDate);
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, myDateSetListener, year, month,
                        day);
        }
        return null;
    }
}


