package ge.bats.myapplication;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ScheduledService extends Service {
    private Timer timer = new Timer();
    static Context globalContext;

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        globalContext = getApplicationContext();
        timer.scheduleAtFixedRate(new TimerTask() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void run() {
                Log.e("aris","service repeated");
             startDownload();
            }
        }, 0, 5*60*1000);//5 Minutes
    }
    public void startDownload() {
        String zone = session.getUser("zone", getApplicationContext());
        Map<String,String> data = new HashMap<>();
        final Connection task = (Connection) new Connection(data).execute("http://31.146.44.97:8080/androidstatusmap?id="+ zone);
        final Handler handler = new Handler(Looper.getMainLooper());
        final Runnable runnable = new Runnable() {
            public void run() {
                String response = task.getResult();
                if(task.isLoaded()){ // just remove call backs
                    handler.removeCallbacks(this);
                    checkData(task.getResult());
                } else { // post again
                    handler.postDelayed(this, 300);
                }
            }
        };
        runnable.run();
    }
    public void checkData(String data) {

        try {
            final JSONArray array = new JSONArray(data);
            final Handler handler3 = new Handler();
            final int total = array.length();

            SQLiteOpenHelper dbHelper = new sql(getApplicationContext());
            final SQLiteDatabase database = dbHelper.getWritableDatabase();


            final Runnable runnable3 = new Runnable() {
                int i = 0;
                int todo1 = 0;
                int todo2 = 0;
                public void run() {
                    if(i < total){ // just remove call backs
                        i++;
                        handler3.postDelayed(this, 10);
                        try {
                            JSONObject jsonObj = array.getJSONObject(i);
                            String code = jsonObj.getString("custid");
                            String status = jsonObj.getString("status");
                            String todo = jsonObj.getString("todo");
                            String q = " WHERE `code`='"+ code +"' AND `todo`!='"+ todo +"' AND `status`!='"+ status +"'";
                            List results =  getResults(q);
                            if(results != null) {
                                if(results.size() > 0) {
                                    if(Integer.valueOf(todo) == 2) {
                                        Log.e("added to todo2", todo);
                                        Log.e("find to todo 2",results.toString());
                                        todo2++;
                                    }
                                    if(Integer.valueOf(todo) == 1) {
                                        Log.e("added to todo1", todo);
                                        Log.e("find to todo 2",results.toString());
                                        todo1++;
                                    }
                                    String query = "UPDATE `reading` SET `todo`='"+ todo +"' WHERE `code`='"+ code +"'";
                                    Log.e("Update",query);
                                    database.execSQL(query);
                                }


                            }

                            //break;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } else { // post again
                        handler3.removeCallbacks(this);
                        database.close();
                        if(todo2 > 0) {
                            startNotification(1,todo2);
                            Log.e("todo2", String.valueOf(todo2));
                        }
                        if(todo1 > 0) {
                            startNotification(2,todo1);
                            Log.e("todo1", String.valueOf(todo1));
                        }
                    }
                }
            };
            runnable3.run();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public static List getResults(String query) {
        SQLiteOpenHelper dbHelper = new sql(globalContext);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

//        String[] columns={sql.TABLE, sql.KEY_ID,sql.KEY_NAME,sql.KEY_CODE,sql.KEY_CID};*/
        Cursor c = database.rawQuery("SELECT * FROM reading "+query,null);
        List<HashMap<String, String>> userList = new ArrayList<>();
        List show = new ArrayList();

        if (c.moveToFirst()) {

            do {
                List list = new ArrayList();
                list.add(c.getString(c.getColumnIndex(sql.KEY_NAME)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_READING)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_CODE)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_METER)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_OREADING)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_price)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_status)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_todo)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_CID)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_own)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_DATE)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_LASTUPDATED)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_COORDS)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_cycle)));

                list.add(c.getString(c.getColumnIndex(sql.KEY_FAVORITE)));
                //  list.add(c.getString(c.getColumnIndex(sql.KEY_zone)));
                show.add(list);
            }

            while (c.moveToNext());
        }
        return show;
    }
    public void startNotification(int type, Integer count) {
        String title = null;
        String message = null;
        String extraTitle = null;
        if(type == 2) {
            extraTitle = "ჩასაჭრელი";
            title = "დაემატა "+ String.valueOf(count) +" ჩასაჭრელი აბონენტი";
            message = "დააწექით რათა ნახოთ ყველა ჩასაჭრელი აბონენტი";
        }
        if(type == 1) {
            extraTitle = "ჩასართველი";
            title = "დაემატა "+ String.valueOf(count) +" ჩასართველი აბონენტი";
            message = "დააწექით რათა ნახოთ ყველა ჩასართველი აბონენტი";
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "bts_00"+ type)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        Intent newIntent = new Intent(getApplicationContext(), MainActivity.class);

        newIntent.putExtra("type","aa");
        newIntent.putExtra("title","bb");

        newIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                getApplicationContext(), 22, newIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(notifyPendingIntent);

        createNotificationChannel(type);
        NotificationManager mNotificationManager=(NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

        // mId allows you to update the notification later on.
        mNotificationManager.notify(type, mBuilder.build());
    }
    private void createNotificationChannel(int type) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "ბათუმის წყალი";
            String description = "ახალი შეტყობინება";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("bts_00"+ type, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);


        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


}
