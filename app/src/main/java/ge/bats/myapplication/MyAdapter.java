package ge.bats.myapplication;

import android.content.ClipData;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.ResourceBundle;

public class MyAdapter extends ArrayAdapter<ClipData.Item> {
    private ArrayList values;
    public MyAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public MyAdapter(Context context, int resource, ArrayList items) {
        super(context,resource,items);
        values = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list, null);
        }
        ArrayList Value = (ArrayList) values.get(position);
        if(Value != null) {
            String Name = Value.get(0).toString();
            String reading = Value.get(1).toString();
            String status = Value.get(6).toString();
            if(Integer.valueOf(status) == 0) {
                ImageView online = (ImageView) v.findViewById(R.id.online);
                online.setVisibility(View.GONE);
            } else {
                Log.e("Status is: ",status);
                ImageView offline = (ImageView) v.findViewById(R.id.offline);
                offline.setVisibility(View.GONE);
            }

            if(reading == "") {
                reading = "ჩვენება არ არის";
            }
            String code = Value.get(2).toString();
            String meter = Value.get(3).toString();



            TextView list_name = (TextView) v.findViewById(R.id.list_name);
            TextView list_code = (TextView) v.findViewById(R.id.list_code);
            TextView list_reading = (TextView) v.findViewById(R.id.list_reading);
            TextView list_meter = (TextView) v.findViewById(R.id.list_meter);
            list_name.setText(Name);
            list_code.setText(code);
            list_reading.setText(reading);
            list_meter.setText(meter);
        }

        return v;
    }

}