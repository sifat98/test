package ge.bats.myapplication;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.provider.SyncStateContract;
import android.util.Log;


public class sql extends SQLiteOpenHelper{
    public final static String DB = "BTS0.sqlite3";
    public final static String TABLE = "reading";
    public final static int DB_VERSION = 1;

    public final static String KEY_ID = "_id";
    public final static String KEY_CODE = "code";
    public final static String KEY_NAME = "name";
    public final static String KEY_CID = "cid";
    public final static String KEY_DATE = "date";
    public final static String KEY_OREADING = "oreading";
    public final static String KEY_READING = "reading";
    public final static String KEY_METER = "meter";
    public final static String KEY_MID = "mid";
    public final static String KEY_status = "status";
    public final static String KEY_todo = "todo";
    public final static String KEY_cycle = "cycle";
    public final static String KEY_price = "price";
    public final static String KEY_own = "own";
    public final static String KEY_upload_status = "upload_status";
    public final static String KEY_LASTUPDATED = "last_updated";
    public final static String KEY_COORDS = "coords";
    public final static String KEY_zone = "zone";
    public final static String KEY_FAVORITE = "favorite";

    public sql(Context context) {
        super(context, DB, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE + "(" +
                KEY_ID + " integer primary key," +
                KEY_NAME + " text," +
                KEY_CODE + " text," +
                KEY_CID + " Integer," +
                KEY_DATE + " DateTime," +
                KEY_OREADING + " Integer," +
                KEY_READING + " Integer," +
                KEY_METER + " text, " +
                KEY_MID + " Integer," +
                KEY_status + " Integer," +
                KEY_todo + " Integer," +
                KEY_cycle + " Integer," +
                KEY_price + " text," +
                KEY_own + " Integer," +
                KEY_FAVORITE + " Integer," +
                KEY_upload_status + " Integer," +
                KEY_LASTUPDATED + " DateTime," +
                KEY_COORDS + " text" +
                ")");

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + DB);
        onCreate(db);
    }
}
