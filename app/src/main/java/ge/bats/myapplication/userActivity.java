package ge.bats.myapplication;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class userActivity extends AppCompatActivity {
    static String getCode;
    static  String reading;
    static String oreading;
    private MenuItem item;
    static Boolean favorite;
    static Menu menu1;
    static Context context;
    static Activity activity;
    Drawable star;

    public Activity getActivity() {
        return activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Intent in = getIntent();
        setTitle(in.getStringExtra("name"));
        getCode = in.getStringExtra("code");
        showUser(getCode);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        context = getApplicationContext();
        activity = getActivity();
        getMenuInflater().inflate(R.menu.user_menu, menu);

        List results = MainActivity.getResults(" WHERE `code`='" + getCode + "'");
        ArrayList user = (ArrayList) results.get(0);


        String st;
        if (user.get(14) == null) {
            st = "0";
        } else {
            st = user.get(14).toString();
        }
        favorite = Boolean.valueOf(st);
        star = ContextCompat.getDrawable(this, android.R.drawable.btn_star_big_off);
        if (favorite) {
            star = ContextCompat.getDrawable(this, android.R.drawable.btn_star_big_on);
        }
        menu1 = (Menu) menu;
        menu1.getItem(0).setIcon(star);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if ( id == android.R.id.home ) {
            finish();
        }
        if( id == R.id.favorite) {
            Boolean toChange = !favorite;
            changeReading(String.valueOf(toChange),getCode,4);
        }

        return super.onOptionsItemSelected(item);
    }




    private void showUser(final String userCode) {
        List results = MainActivity.getResults(" WHERE `code`='"+ userCode+"'");
        ArrayList user = (ArrayList) results.get(0);

        TextView user_code = (TextView) findViewById(R.id.user_code);
        TextView user_meter = (TextView) findViewById(R.id.user_meter);
        TextView user_price = (TextView) findViewById(R.id.user_price);
        TextView user_reading = (TextView) findViewById(R.id.user_reading);
        TextView user_oreading = (TextView) findViewById(R.id.user_oreading);
        TextView user_rate = (TextView) findViewById(R.id.user_rate);
        TextView user_status = (TextView) findViewById(R.id.user_status);

        Button user_button = (Button) findViewById(R.id.user_button);

        final String code = user.get(2).toString();
        String meter = user.get(3).toString();
        String price = user.get(5).toString();
        reading = user.get(1).toString();
        oreading = user.get(4).toString();
        String status = user.get(6).toString();
        String todo = user.get(7).toString();

        if(user.get(14) == null) {
            star = ContextCompat.getDrawable(this, android.R.drawable.btn_star_big_off);
        } else {
            String fav = user.get(14).toString();
            if(Boolean.valueOf(fav) == true) {
                star = ContextCompat.getDrawable(this,android.R.drawable.btn_star_big_on);
            } else {
                star = ContextCompat.getDrawable(this, android.R.drawable.btn_star_big_off);
            }
        }
        if(menu1 != null) {
            menu1.getItem(0).setIcon(star);
        }

        Boolean update = false;
        String statusText;
        String buttonText;

        Integer ifTodo = Integer.valueOf(todo);
        Integer ifStatus = Integer.valueOf(status);

        Log.e(todo,status);
        if(ifTodo == 0 && ifStatus == 0 || ifTodo == 2 && ifStatus == 2) {
            user_price.setBackgroundColor(Color.parseColor("#10C916"));
        } else {
            user_price.setBackgroundColor(Color.RED);
        }
        if(ifTodo == 0) {
            statusText = "აქტიური";
            user_button.setVisibility(View.GONE);

            user_status.setText(statusText);
            user_status.setTextColor(Color.parseColor("#10C916"));
        } else {
            user_button.setVisibility(View.VISIBLE);
        }
        if(ifStatus == 2 && ifTodo == 0) {
            statusText = "ჩაჭრილი";
            user_status.setText(statusText);
            user_status.setTextColor(Color.RED);
        }
        if(ifTodo == 1) {
            statusText = "ჩასაჭრელი";
            buttonText = "ჩაჭრა";
            user_button.setBackgroundColor(Color.RED);
            user_button.setText(buttonText);
            user_status.setText(statusText);
            user_status.setTextColor(Color.RED);
            user_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(userActivity.this)
                            .setTitle("ყურადღება")
                            .setMessage("ნამდვილად გსურთ აბონენტის ჩაჭრა?")
                            .setPositiveButton("შენახვა", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(userActivity.this, "აბონენტი ჩაიჭრა წარმატებით!", Toast.LENGTH_SHORT).show();
                                    changeReading("0",code,2);
                                }})
                            .setNegativeButton("გაუქმება", null).show();
                }
            });
        }
        if(ifTodo == 2) {
            statusText = "ჩასართველი";
            buttonText = "ჩართვა";
            user_button.setBackgroundColor(Color.parseColor("#10C916"));
            user_button.setText(buttonText);
            user_status.setText(statusText);
            user_status.setTextColor(Color.RED);

            user_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(userActivity.this)
                            .setTitle("ყურადღება")
                            .setMessage("ნამდვილად გსურთ აბონენტის ჩართვა?")
                            .setPositiveButton("შენახვა", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(userActivity.this, "აბონენტი ჩაირთო წარმატებით!", Toast.LENGTH_SHORT).show();
                                    changeReading("0",code,3);
                                }})
                            .setNegativeButton("გაუქმება", null).show();
                }
            });


        }

        String rate = "0";
        if(reading.length() > 0) {
            Integer ireading = Integer.valueOf(reading);
            Integer ioreading = Integer.valueOf(oreading);
            if(ireading > 0 && ioreading > 0) {
                rate  = String.valueOf(ireading - ioreading);
            }
            if (Integer.valueOf(oreading) > Integer.valueOf(reading)) {
                rate = String.valueOf(Integer.valueOf(oreading) - Integer.valueOf(reading));
            } else {
                rate = String.valueOf(Integer.valueOf(reading) - Integer.valueOf(oreading));
            }
        } else {
            reading = "შეცვლა...";
        }



        user_reading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(userActivity.this);
                final EditText input = new EditText(userActivity.this);
                input.setHint(reading);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setRawInputType(Configuration.KEYBOARD_12KEY);
                builder
                        .setTitle("ახალი ჩვენება")
                        .setView(input)
                        .setPositiveButton("შენახვა", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                String value = input.getText().toString();
                                if (input.getText().toString().trim().length() == 0) {
                                    session.toast("არ შეიძლება იყოს ცარიელი",getApplicationContext());
                                } else {
                                  changeReading(value,userCode,1);
                                }
                                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                            }
                        })
                        .setNegativeButton("გაუქმება", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                            }

                        });

                builder.show();
                input.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

            }
        });







        user_code.setText(code);
        user_meter.setText(meter);
        user_price.setText(price+ " ₾");
        user_reading.setText(reading);
        user_oreading.setText(oreading);
        user_rate.setText(rate+" ტ.");

        final List resulte = MainActivity.getResults(" WHERE `own`!='0' AND `upload_status`='0'");
        setMenuCounter(resulte.size());

    }

    public void setMenuCounter(int count) {
        TextView need = (TextView) MainActivity.globalActivity.findViewById(R.id.counter);
        if(count > 0) {

            need.setText(String.valueOf(count));
        } else {
            need.setText("");
        }
    }

    public void changeReading(String value, String code, Integer type) {
        if(type == 1) {
            TextView user_reading = (TextView) findViewById(R.id.user_reading);
            TextView user_rate = (TextView) findViewById(R.id.user_rate);
            user_reading.setText(value);
            String rate = null;
            Integer ioreading = Integer.valueOf(oreading);
            if (ioreading > Integer.valueOf(value)) {
                rate = String.valueOf(ioreading - Integer.valueOf(value));
            }
            if (value.length() < oreading.length() && ioreading < Integer.valueOf(value)) {
                int OldLength = oreading.length();
                int curLength = value.length();
                String defaultRate = "100000000000000000";
                defaultRate = defaultRate.substring(0, OldLength + 1);
                int nextRate = Integer.valueOf(defaultRate) + Integer.valueOf(value);


                rate = String.valueOf(nextRate - ioreading);

            } else {
                if (rate == null) {
                    rate = String.valueOf(Integer.valueOf(value) - Integer.valueOf(oreading));
                }
            }
            user_rate.setText(rate + " ტ.");
            Date currentTime = Calendar.getInstance().getTime();
            String updated = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(currentTime);
            String date = new SimpleDateFormat("yyyy-MM-dd").format(currentTime);
            GPSTracker gps = new GPSTracker(getApplicationContext());
            String coords = Double.toString(gps.getLatitude()) + ":" + Double.toString(gps.getLongitude());
            String query = "UPDATE `reading` set " +
                    "`reading`='" + value + "'," +
                    "`own`='1'," +
                    "`last_updated`='" + updated + "'," +
                    "`date`='" + date + "'," +
                    "`upload_status`='0'," +
                    "`coords`='" + coords + "' " +
                    "WHERE `code`='" + code + "'";
            SQLiteOpenHelper dbHelper = new sql(getApplicationContext());
            final SQLiteDatabase database = dbHelper.getWritableDatabase();
            database.execSQL(query);
            database.close();
            showUser(code);
        }

        if(type == 2) {
            Button user_button = (Button) findViewById(R.id.user_button);
            user_button.setVisibility(View.GONE);

            String statusText = "ჩაჭრილი";
            user_button.setVisibility(View.GONE);
            TextView user_status = (TextView) findViewById(R.id.user_status);
            user_status.setText(statusText);
            user_status.setTextColor(Color.RED);

            Date currentTime = Calendar.getInstance().getTime();
            String updated = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(currentTime);

            String query = "UPDATE `reading` set " +
                    "`reading`='" + value + "'," +
                    "`own`='2'," +
                    "`last_updated`='" + updated + "'," +
                    "`todo`='0'," +
                    "`upload_status`='0'," +
                    "`status`='2'" +
                    "WHERE `code`='" + code + "'";
            SQLiteOpenHelper dbHelper = new sql(getApplicationContext());
            final SQLiteDatabase database = dbHelper.getWritableDatabase();
            database.execSQL(query);
            database.close();
            showUser(code);
        }

        if(type == 3) {
            Button user_button = (Button) findViewById(R.id.user_button);
            user_button.setVisibility(View.GONE);

            String statusText = "აქტიური";
            user_button.setVisibility(View.GONE);
            TextView user_status = (TextView) findViewById(R.id.user_status);
            user_status.setText(statusText);
            user_status.setTextColor(Color.parseColor("#10C916"));

            Date currentTime = Calendar.getInstance().getTime();
            String updated = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(currentTime);

            String query = "UPDATE `reading` set " +
                    "`reading`='" + value + "'," +
                    "`own`='3'," +
                    "`last_updated`='" + updated + "'," +
                    "`todo`='0'," +
                    "`upload_status`='0'," +
                    "`status`='0'" +
                    "WHERE `code`='" + code + "'";
            SQLiteOpenHelper dbHelper = new sql(getApplicationContext());
            final SQLiteDatabase database = dbHelper.getWritableDatabase();
            database.execSQL(query);
            database.close();
            showUser(code);
        }
        if(type == 4) {

            String query = "UPDATE `reading` set " +
                    "`favorite`='" + value + "'" +
                    "WHERE `code`='" + code + "'";
            SQLiteOpenHelper dbHelper = new sql(getApplicationContext());
            final SQLiteDatabase database = dbHelper.getWritableDatabase();
            database.execSQL(query);
            database.close();
            favorite = !favorite;
            showUser(getCode);

        }
    }
    @Override
    public void onBackPressed() {
        this.finish();
    }


}
