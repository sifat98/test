package ge.bats.myapplication;

import android.content.Intent;
import android.os.IBinder;

abstract class Service {
    public abstract IBinder onBind(Intent intent);
}
