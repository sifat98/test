package ge.bats.myapplication;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;

public class bts {
    public static Activity globalActivity;
    public static Context globalContext;
    Boolean Loaded = false;
    public  static String coords = "0.0:0.0";
    public static void downloadList(Context context, Activity activity) {
        globalContext = context;
        globalActivity = activity;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            GPSTracker gps = new GPSTracker(context);
            coords = Double.toString(gps.getLatitude()) + ":"+ Double.toString(gps.getLongitude());
        } else {
            ActivityCompat.requestPermissions(activity, new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION },0);
        }

        String zone = session.getUser("zone", context);
        Map<String,String> data = new HashMap<>();
        final Connection task = (Connection) new Connection(data).execute("http://31.146.44.97:8080/androidmap?id="+ zone);
        final Handler handler = new Handler();
        Intent start = new Intent(context,Loading.class);
        showProgress(true,activity,"მონაცემების ჩამოტვირთვა...\n       გთხოვთ დაელოდოთ");
        final Runnable runnable = new Runnable() {
            public void run() {
                String response = task.getResult();
                if(task.isLoaded()){ // just remove call backs
                    handler.removeCallbacks(this);
                    showProgress(false,globalActivity,"");
                    // Log.e("data",task.getResult());
                    saveData(task.getResult(),globalContext);
                } else { // post again
                    handler.postDelayed(this, 300);
                }
            }
        };
        runnable.run();
    }
    public static void xuiznaet() {

    }

    public static void satestoDevkomanda() {

    }

    public static void undaDavabruno() {

    }


    public static void saveData(String data,Context context) {
        showBar(true);

        SQLiteOpenHelper dbHelper = new sql(context);
        final SQLiteDatabase database = dbHelper.getWritableDatabase();

       /* ContentValues values = new ContentValues();
        values.put(sql.KEY_ID,"3");
        values.put(sql.KEY_NAME,"test");
        values.put(sql.KEY_CODE,"123");
        values.put(sql.KEY_CID,"123");
        values.put(sql.KEY_DATE,"123");
        values.put(sql.KEY_OREADING,"123");
        values.put(sql.KEY_READING,"123");
        values.put(sql.KEY_METER,"123");
        values.put(sql.KEY_MID,"123");
        values.put(sql.KEY_LASTUPDATED,"");
        values.put(sql.KEY_COORDS,"");
        database.insert(sql.TABLE,null,values);*/
       // Log.e("Inserting values",values.toString());



        try {
            final JSONArray array = new JSONArray(data);

            final Handler handler = new Handler();

            final int total = array.length();


            showProgress(false,globalActivity,"");
            final Runnable runnable = new Runnable() {
                int i = 0;
                public void run() {
                    if(i < total){ // just remove call backs
                        i++;
                        handler.postDelayed(this, 0);
                        setProgress(total,i);

                        try {
                            JSONObject jsonObj = array.getJSONObject(i);
                            String id = jsonObj.getString("id");
                            String name =  jsonObj.getString("name");
                            String code = jsonObj.getString("code");
                            String cid = jsonObj.getString("cid");
                            String date = jsonObj.getString("date");
                            String oreading = jsonObj.getString("oraiding");
                            String reading = jsonObj.getString("reading");
                            String meter = jsonObj.getString("meter");
                            String mid = jsonObj.getString("mid");
                            String status = jsonObj.getString("status");
                            String todo = jsonObj.getString("todo");
                            String cycle = jsonObj.getString("cycle");
                            String price = jsonObj.getString("topay");

                            ContentValues values = new ContentValues();
                            values.put(sql.KEY_ID,id);
                            values.put(sql.KEY_NAME,name);
                            values.put(sql.KEY_CODE,code);
                            values.put(sql.KEY_CID,cid);
                            values.put(sql.KEY_DATE,date);
                            values.put(sql.KEY_OREADING,oreading);
                            values.put(sql.KEY_READING,reading);
                            values.put(sql.KEY_METER,meter);
                            values.put(sql.KEY_MID,mid);
                            values.put(sql.KEY_status,status);
                            values.put(sql.KEY_todo,todo);
                            values.put(sql.KEY_cycle,cycle);
                            values.put(sql.KEY_price,price);
                            values.put(sql.KEY_LASTUPDATED,"");
                            values.put(sql.KEY_COORDS,"");
                            database.insert(sql.TABLE,null,values);
                            setProgress(total,(i+1));
                            //break;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } else { // post again
                        handler.removeCallbacks(this);
                        showBar(false);
                        database.close();
                        MainActivity.startDraw(null);
                    }
                }
            };
            runnable.run();

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public static void setProgress(Integer total, Integer current) {
        Integer erti = current * 100;
        Integer percent = erti / total;

        TextView xviti = (TextView)(globalActivity).findViewById(R.id.xviti);
        ProgressBar bar = (ProgressBar)(globalActivity).findViewById(R.id.progress_bar);
        TextView gvriti = (TextView)(globalActivity).findViewById(R.id.gvriti);

        String xvitiMSG = String.valueOf(current) + "/"+ String.valueOf(total);
        String gvritiMSG = String.valueOf(percent) + "%";
        xviti.setText(xvitiMSG);
        gvriti.setText(gvritiMSG);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            bar.setProgress(percent,true);
        }
    }


    public static void showProgress(boolean type, Activity activity, String message) {
        RelativeLayout loading = (RelativeLayout)(activity).findViewById(R.id.loading_bar);
        TextView text = (TextView)(activity).findViewById(R.id.loading_message);
        text.setText(message);
        if(type) {
            loading.setVisibility(View.VISIBLE);
        } else {
            loading.setVisibility(View.GONE);
        }
    }
    public static void showBar(boolean type) {
        RelativeLayout progress = (RelativeLayout)(globalActivity).findViewById(R.id.progress_layout);
        if(type) {
            progress.setVisibility(View.VISIBLE);
        } else {
            progress.setVisibility(View.GONE);
        }
    }

}
