package ge.bats.myapplication;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


class Connection extends AsyncTask<String, Void, Void> {
    final Map<String,String> posData;
    public Connection(Map<String,String> data) {

        posData = data;
    }
    String response = null;
    HttpURLConnection connection;
    String globalResult;
    Boolean Loaded = false;
    static final int statusCode = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected Void doInBackground(String... strings) {
        try {
            Log.e("test",posData.toString());
            URL url = new URL(strings[0]);

            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Connection","Keep-alive");
            connection.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            connection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
            connection.setRequestProperty("X-CSRF-Token", "hj96McoIc5v11s0ZuUezcn39uWqhS04NQCv40huyDHvIISd2aFMZs9vsIR54pVHu");


            if(posData.size() > 0) {
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.connect();
                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                String data = "";
                for (Map.Entry<String, String> e :  posData.entrySet()) {
                    String key    = e.getKey();
                    String value  = e.getValue();
                    data +=  key + "="+ value+"&";
                }
                wr.write(data);
                wr.flush();
            } else {
                connection.setRequestMethod("GET");
                connection.connect();
            }




            int statusCode = connection.getResponseCode();
           // Log.e("StatusCode",String.valueOf(statusCode));
            if (statusCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                StringBuffer buffer = new StringBuffer();
                while((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String response = buffer.toString();
                setResult(response);
            }
            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getResult() {
        return globalResult;
    }
    public Boolean isLoaded() {
        return Loaded;
    }
    public void setResult(String result) {
        globalResult = result;
        Loaded = true;
    }
}
