package ge.bats.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity
        extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
,NumberPicker.OnValueChangeListener
{
    public static Activity globalActivity;
    static Boolean toggled = false;
    static Context context;
    static ListView reading_list;
    static RelativeLayout download;
    static boolean isLoaded = false;
    static boolean isShow = false;
    static String oldSearch = null;
    static Integer oldPosition = 0;
    static String oldName = "ყველა აბონენტი";;
    static Activity activity;

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {

         String zone = String.valueOf(numberPicker.getValue());
        String obj = session.getDefaults("user",context);
        try {
            JSONObject json = new JSONObject(obj);
            json.put("zone",zone);
            session.setDefaults("user",json.toString(),context);
            Intent in = getIntent();
            finish();
            startActivity(in);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        globalActivity = this;







        setSupportActionBar(toolbar);
        startService(new Intent(this, MyLocationService.class));
        context = MainActivity.this;
        download = (RelativeLayout) findViewById(R.id.download);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.bottom_menu);



        final SwipeRefreshLayout swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.refresher);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // new Upload().Start("263/22-4");
                        startDraw(oldSearch);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 100);
            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.bottom_nav);
        navigationView.setNavigationItemSelectedListener(this);


        Button btn_download = (Button) findViewById(R.id.download1);
        btn_download.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                bts.downloadList(getApplicationContext(), MainActivity.this);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setRotation(0);
                view.animate().rotation(180);
                menuToggle();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView nav = findViewById(R.id.nav_view);
        nav.setNavigationItemSelectedListener(this);
        reading_list = (ListView) findViewById(R.id.reading_list);



        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            String query = getIntent().getStringExtra(SearchManager.QUERY);
            String select = "where meter LIKE '"+ query +"%' OR code LIKE '"+ query +"%' OR name LIKE '"+ query +"%'";
            startDraw(select);
            oldSearch = select;


        } else {

            startDraw(oldSearch);
            setTitle(oldName);
            if(oldPosition > 0) {

                ListView reading_list = (ListView) findViewById(R.id.reading_list);
                reading_list.setSelection(oldPosition);

            }
        }

        Intent i= new Intent(context, ScheduledService.class);
        context.startService(i);
    };


    @Override
    public void onNewIntent(Intent intent) {
        Intent in = intent;
        Log.e("new", String.valueOf(in.getParcelableExtra("type")));
    }



    public static List getResults(String query) {
        SQLiteOpenHelper dbHelper = new sql(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

//        String[] columns={sql.TABLE, sql.KEY_ID,sql.KEY_NAME,sql.KEY_CODE,sql.KEY_CID};*/
        Cursor c = database.rawQuery("SELECT * FROM reading "+query,null);
        List<HashMap<String, String>> userList = new ArrayList<>();
        List show = new ArrayList();

        if (c.moveToFirst()) {

            do {
                List list = new ArrayList();
                list.add(c.getString(c.getColumnIndex(sql.KEY_NAME)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_READING)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_CODE)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_METER)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_OREADING)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_price)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_status)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_todo)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_CID)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_own)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_DATE)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_LASTUPDATED)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_COORDS)));
                list.add(c.getString(c.getColumnIndex(sql.KEY_cycle)));

                list.add(c.getString(c.getColumnIndex(sql.KEY_FAVORITE)));
            //  list.add(c.getString(c.getColumnIndex(sql.KEY_zone)));
                show.add(list);
            }

            while (c.moveToNext());
        }
        return show;
    }
    public static void startDraw(String query) {
        if(query == null) {
            query = "LIMIT 30";
        }



        List show = getResults(query);
        if(show.size() > 0) {
            download.setVisibility(View.GONE);
            reading_list.setVisibility(View.VISIBLE);
            MyAdapter adapter = new MyAdapter(context,R.layout.list,(ArrayList) show);
            reading_list.setAdapter(adapter);
            reading_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                 Intent start = new Intent(globalActivity,userActivity.class);

                 TextView list_code = (TextView) view.findViewById(R.id.list_code);
                 TextView list_name = (TextView) view.findViewById(R.id.list_name);
                 String code = list_code.getText().toString();
                 String name = list_name.getText().toString();

                 Log.e("name",name);
                 start.putExtra("name",name);
                 start.putExtra("code",code);

                 oldPosition = position;

                 globalActivity.startActivity(start);


                }
            });
        } else {
            download.setVisibility(View.VISIBLE);
            reading_list.setVisibility(View.GONE);
        }


    }
    @Override
    public void onStart() {

        super.onStart();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void showNumberPicker(View view){
        NumberPickerDialog newFragment = new NumberPickerDialog();
        newFragment.setValueChangeListener((NumberPicker.OnValueChangeListener) this);
        newFragment.show(getSupportFragmentManager(), "time picker");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // Context context = getApplicationContext();
       // getMenuInflater().inflate(R.menu.main, menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        final List results = MainActivity.getResults(" WHERE `own`!='0' AND `upload_status`='0'");
        setMenuCounter(results.size());

        FloatingActionButton changeZone = (FloatingActionButton) findViewById(R.id.changeZone);
        changeZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberPicker(v);
            }
        });


        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        MenuItem item = menu.findItem(R.id.search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(query.length() > 0) {
                    startDraw("where meter LIKE '" + query + "%' OR code LIKE '" + query + "%' OR name LIKE '" + query + "%' LIMIT 10");
                } else {
                    startDraw(null);
                }
                return false;
            }
        });

        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                searchView.setIconifiedByDefault(true);
                searchView.setFocusable(true);
                searchView.setIconified(false);
                if(!isShow) {

                    // searchView.requestFocusFromTouch()
                    isShow = true;
                }



                return false;
            }
        });


        String name = session.getUser("name", context) + " " + session.getUser("last", context);
        TextView bts_name = findViewById(R.id.bts_name);
        bts_name.setText(name);

        TextView bts_zone = findViewById(R.id.bts_zone);
        bts_zone.setText("არჩეული უბანი " + session.getUser("zone", context));
        if(!isLoaded) {
            session.toast("გამარჯობა: " + name, context);
            isLoaded = !isLoaded;
        }



        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settings = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settings);
        }




        return super.onOptionsItemSelected(item);
    }
    public void setMenuCounter(int count) {
        TextView need = (TextView) globalActivity.findViewById(R.id.counter);
        if(count > 0) {

            need.setText(String.valueOf(count));
        } else {
            need.setText("");
        }
    }

    static void menuToggle() {
        Integer goTo;
        int Visability = View.GONE;

        RelativeLayout bottom_list = (RelativeLayout) globalActivity.findViewById(R.id.bottom_l);

        LinearLayout mask = (LinearLayout) globalActivity.findViewById(R.id.mask);
        mask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuToggle();
            }
        });
        if(!toggled) {
            goTo = 0;
            Visability = View.VISIBLE;
        } else {
            Visability = View.GONE;
            goTo = 800;
        }
        mask.setVisibility(Visability);
        toggled = !toggled;
        bottom_list.animate().translationY(goTo);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        int id = item.getItemId();

        if(id == R.id.show_all) {
            String query = null;
            String name = "ყველა აბონენტი";
            oldSearch = query;

            startDraw(query);
            menuToggle();
            oldName = name;
            setTitle(name);
        }
        if(id == R.id.show_reading) {

            String query = " WHERE `reading`=''";
            String name = "ჩვენების გარეშე";
            oldSearch = query;
            startDraw(query);
            oldName = name;
            setTitle(name);
            menuToggle();
        }
        if(id == R.id.show_todisable) {
            String query = " WHERE `todo`='1' AND `price`>='1'";
            String name = "ჩასაჭრელი";
            oldSearch = query;
            startDraw(query);
            menuToggle();
            oldName = name;
            setTitle(name);
        }
        if(id == R.id.show_toenable) {
            String query = " WHERE `todo`='2'";
            String name = "ჩასართველი";
            oldSearch = query;
            oldName = name;
            startDraw(query);
            setTitle(name);
            menuToggle();
        }
        if (id == R.id.download) {




         bts.downloadList(getApplicationContext(), MainActivity.this);

        } else if (id == R.id.upload) {
            new Upload("").findAll(getApplicationContext(), MainActivity.this);
        } else if(id == R.id.history) {

            Intent history = new Intent(MainActivity.this,historyActivity.class);
            startActivity(history);
        } else if (id == R.id.nav_share) {
            session.setDefaults("user","",getApplicationContext());
            Intent login = new Intent(MainActivity.this,Login.class);
            startActivity(login);
        }
        else if(id == R.id.favorites) {
            String query = " WHERE `favorite`='true'";
            oldSearch = query;
            oldName = "რჩეულები";
            setTitle(oldName);
            startDraw(query);
        }
        else if(id == R.id.dropall) {
            new AlertDialog.Builder(this)
                    .setTitle("ყურადღება!")
                    .setMessage("ნამდვილად გსურთ მონაცემთა ბაზის გასუფთავება?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton("კი", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                            SQLiteOpenHelper dbHelper = new sql(getApplicationContext());
                            final SQLiteDatabase database = dbHelper.getWritableDatabase();
                            database.execSQL("DELETE FROM "+ sql.TABLE);
                            database.close();
                            finish();
                            startActivity(getIntent());
                        }})
                    .setNegativeButton("არა", null).show();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }


}
